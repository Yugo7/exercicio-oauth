package br.com.itau.authentication.repositories;

import br.com.itau.authentication.models.Contato;
import org.springframework.data.repository.CrudRepository;

public interface ContatoRepository extends CrudRepository<Contato, Long> {
}
