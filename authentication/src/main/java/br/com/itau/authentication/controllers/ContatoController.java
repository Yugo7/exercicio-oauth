package br.com.itau.authentication.controllers;

import br.com.itau.authentication.models.Contato;
import br.com.itau.authentication.models.DTO.ContatoRequestDTO;
import br.com.itau.authentication.security.Usuario;
import br.com.itau.authentication.services.ContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

@RestController
public class ContatoController {

    @Autowired
    private ContatoService contatoService;

    @PostMapping
    public Contato create(@RequestBody ContatoRequestDTO contato, @AuthenticationPrincipal Usuario usuario) {
        return  contato;
    }
}